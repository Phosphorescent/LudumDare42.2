﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class PointManager {
	public static bool preGame = true;

    public static float gameStartTime = 0f;
    public static int points = 0;
    public static float publicOpinion = 0.75f;


    public static void StartGame() {
        Time.timeScale = 1.0f;
    }

    public static void StopGame()
    {
        Time.timeScale = 0.0f;
    }


    public static void ResetStats()
    {
        points = 0;
        publicOpinion = 0.75f;
        gameStartTime = Time.time;
		preGame = true;
	}
}


public class UIManager : MonoBehaviour {

    public GameObject bgMusicPrefab;

    public GameObject endGameScreen;
    public Text endScoreText;
    public Text endHighscoreText;

    public GameObject crosshair;

    public Image waterHeightBar;
    public Text pointText;
    public GameObject Ocean;
    private TideController tc;

    public PlayerController pc;

    public Slider sensitivitySlider;

    public Text highScoreText;
    private int highScore;
    private int initHighScore;

    public GameObject menu;
    private bool menuIsActive;
    public GameObject optionsMenu;

	public Image publicOpinionBar;
	public Color publicOpinionStart;
	public Color publicOpinionEnd;

	public GameObject timerScreen;
	public Text timerText;	

	private float startTime = 0.0f;

	// Use this for initialization
	void Start () {

        PointManager.ResetStats();
        crosshair.SetActive(true);

        endGameScreen.SetActive(false);

        if(PlayerPrefs.HasKey("customSens"))
        {
            sensitivitySlider.value = PlayerPrefs.GetFloat("customSens");
        } else
        {
            sensitivitySlider.value = pc.lookSpeed;
        }

        tc = Ocean.GetComponent<TideController>();

		PointManager.StartGame();

        menuIsActive = false;

        if(PlayerPrefs.HasKey("Highscore"))
        {
            highScore = PlayerPrefs.GetInt("Highscore");
            initHighScore = highScore;
            highScoreText.text = highScore.ToString();
        } else
        {
            PlayerPrefs.SetInt("Highscore", 0);
        }

		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {

        EndGame();

		pointText.text = PointManager.points.ToString();

        levelSet(tc.tideWeight);

		PointManager.publicOpinion = (PointManager.publicOpinion > 1) ? 1 : ((PointManager.publicOpinion < 0) ? 0 : PointManager.publicOpinion);
		publicOpinionSet(PointManager.publicOpinion);

        float countdown = 5 - (Time.time - startTime);

        if (Input.GetKeyDown("escape") && countdown < -0.6){

            if (!menuIsActive)
            {
                crosshair.SetActive(false);
                menu.SetActive(true);
                menuIsActive = true;
                PointManager.preGame = true;
                PointManager.StopGame();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

            } else
            {
                crosshair.SetActive(true);
                menu.SetActive(false);
                menuIsActive = false;
                PointManager.preGame = false;
                PointManager.StartGame();
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false; 
            }

        }

        if(PointManager.points > highScore)
        {
            highScore = PointManager.points;
            highScoreText.text = highScore.ToString();
        }


		if (countdown > 0) {
			timerText.text = countdown.ToString("F2");
		} else if ( countdown > -0.5 ) {
			timerText.text = "Go!";
		} else if (PointManager.preGame && countdown > -0.6) {
			timerScreen.SetActive(false);
			PointManager.preGame = false;
			PointManager.gameStartTime = Time.time;
            GameObject bgMusic = GameObject.Instantiate(bgMusicPrefab, transform.position, Quaternion.identity);
            Debug.Log("Spawning");
		}
	}

    void levelSet(float val)
    {
		waterHeightBar.fillAmount = val;
    }

	void publicOpinionSet(float val)
	{
		publicOpinionBar.fillAmount = val;
		publicOpinionBar.color = Color.Lerp(publicOpinionEnd, publicOpinionStart, PointManager.publicOpinion);
	}

    public void ResumeGame()
    {
        crosshair.SetActive(true);
        menu.SetActive(false);
        menuIsActive = false;
        PointManager.preGame = false;
        PointManager.StartGame();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }

    public void enterOptions()
    {
        optionsMenu.SetActive(true);
        menu.SetActive(false);
    }

    public void returnFromOptions()
    {
        optionsMenu.SetActive(false);
        menu.SetActive(true);
    }

    public void applySettings()
    {

        Debug.Log(sensitivitySlider.value);

        if(sensitivitySlider.value != pc.lookSpeed)
        {
            pc.lookSpeed = sensitivitySlider.value;
            PlayerPrefs.SetFloat("customSens", sensitivitySlider.value);
        }

    }

    public void ReturnToMenu()
    {

        SceneManager.LoadScene("Menu Screen");

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void EndGame()
    {
        if (tc.tideWeight >= 1f || PointManager.publicOpinion <= 0)
        {
            PointManager.StopGame();
            EndGameScreen();
        }
    }

    void EndGameScreen()
    {
        PointManager.preGame = true;
        crosshair.SetActive(false);
        endScoreText.text = PointManager.points.ToString();
        endHighscoreText.text = highScore.ToString();
        endGameScreen.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        PlayerPrefs.SetInt("Highscore", highScore);
    }

    public void Retry()
    {
        SceneManager.LoadScene("Main");
    }
}
