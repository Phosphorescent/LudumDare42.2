﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeController : MonoBehaviour {

    public GameObject grenade;
    public GameObject grenadeStart;
    public float throwForce;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void throwGrenade(Transform cameraTransform)
    {
        GameObject grenadeInstance = Instantiate(grenade, grenadeStart.transform.position, Quaternion.identity);
        Rigidbody rb = grenadeInstance.GetComponent<Rigidbody>();
        rb.AddForce(cameraTransform.forward * throwForce);
    }
}
