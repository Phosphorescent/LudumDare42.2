﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRigidbodyController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		foreach( Material material in gameObject.GetComponent<Renderer>().materials ) {
			if (material.name == "RandomColour (Instance)") material.color = Random.ColorHSV(0f, 1f, 0.5f, 1f, 0.5f, 1f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Beach")
        {

            Rigidbody bod = gameObject.GetComponent<Rigidbody>();

            bod.constraints = RigidbodyConstraints.FreezeAll;

            Transform bigCol = transform.Find("collider");
            if(bigCol != null)
            {
                bigCol.gameObject.SetActive(true);
            }
        }
    }
}
