﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPickupController : MonoBehaviour {

    private bool isPressed;

    public string[] abilities;

    public PlayerController pc;

	public Animation clawAnimation;

    public AudioSource abilityPickup;
    public AudioSource trashPickup;

	// Use this for initialization
	void Start () {
        
	}

	
	// Update is called once per frame
	void Update () {


        if (Input.GetAxisRaw("Fire1") == 1 && !isPressed)
        {
            isPressed = true;

            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 50f))
            {
                if (hit.transform.gameObject.tag == "Trash")
                {
                    trashPickup.Play(0);
                    Destroy(hit.transform.gameObject);
                    PointManager.points += 1;
                    PointManager.publicOpinion += 0.02f;

					clawAnimation.Play();
				}
                else if (hit.transform.gameObject.tag == "Ability")
                {
                    abilityPickup.Play(0);
                    Destroy(hit.transform.gameObject);
                    PickupAbility();

					clawAnimation.Play();
				}
            }
        }

        if(Input.GetAxisRaw("Fire1") == 0)
        {
            isPressed = false;
        }
		
	}

    void PickupAbility()
    {
        if (pc.currentAbility == "")
        {
            pc.currentAbility = abilities[Random.Range(0, abilities.Length)];
            Debug.Log(pc.currentAbility);
        }

    }
}
