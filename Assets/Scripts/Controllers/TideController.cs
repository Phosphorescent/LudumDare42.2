﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TideController : MonoBehaviour {

    public float tideSpeed = 1f;
	public float tideLowerY = -110f;
	public float tideUpperY = -45f;
	public float tideWeight = 0f;

    private float tideDiff;
    private float currentDiff;
    private float dist;

    public GameObject waterSfx;

    private void Start()
    {
        tideDiff = tideUpperY - tideLowerY;
        dist = tideDiff * 0.05f;
    }

    // Update is called once per frame
    void Update () {
		if (PointManager.preGame) return;
        float time = Time.time - PointManager.gameStartTime;

        float changeY = yChange(tideSpeed, time);
        changeY = (float) 0.01 * changeY;

        transform.Translate(0, changeY, 0);

        // Calculate tideWeight
        currentDiff = transform.position.y - tideLowerY;

        tideWeight = currentDiff / tideDiff;
	}

    float yChange(float tideSpeed, float time)
    {
        return (tideSpeed + Mathf.Cos(time));
    }

    public void lowerTide()
    {
        GameObject _ = GameObject.Instantiate(waterSfx, transform.position, Quaternion.identity);

        Vector3 newPos;
        // Lower the tide
        if (currentDiff < dist)
        {
            newPos = new Vector3(0, tideLowerY, 0);
            transform.position = newPos;
        } else {
            transform.Translate(0, -dist, 0);
        }

        Destroy(_, 2f);
    }
}
