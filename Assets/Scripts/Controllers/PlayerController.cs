﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	public float speed = 15f;
	public float lookLimits = 80f;

	public float jumpSpeed = 10f;
	public float gravity = 20f;

    public float lookSpeed = 1f;

    public string currentAbility = "";

    public Text notification;

    public GameObject ocean;

	private CharacterController characterController;
	private Transform characterCamera;
	private Vector3 moveDirection = Vector3.zero;
    private GrenadeController grenadeController;
    private TideController tideController;

	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}

	void Start () {

        if (PlayerPrefs.HasKey("customSens"))
        {
            lookSpeed = PlayerPrefs.GetFloat("customSens");
        }

		characterController = GetComponent<CharacterController>();
		characterCamera = transform.GetChild(0);

        grenadeController = GetComponent<GrenadeController>();
        tideController = ocean.gameObject.GetComponent<TideController>();

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	float xAngle = 0f;
	float yAngle = 0f;

	void Update () {
		if (PointManager.preGame) return;

		xAngle += Input.GetAxis("Mouse X") * lookSpeed;
		yAngle -= Input.GetAxis("Mouse Y") * lookSpeed;

		characterCamera.eulerAngles = new Vector3(ClampAngle(yAngle, -lookLimits, lookLimits), characterCamera.eulerAngles.y, characterCamera.eulerAngles.z);
		transform.eulerAngles = new Vector3(0, xAngle, 0);

        if (Input.GetAxisRaw("Fire2") > 0)
        {
            doAbility();
        }

        if(currentAbility != "")
        {
            notification.text = currentAbility.ToUpper();
        } else
        {
            notification.text = "NONE";
        }

	}

	void FixedUpdate () {
		if (PointManager.preGame) return;

		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");

		Vector3 initialDirection = new Vector3(horizontal, 0, vertical);
		initialDirection = transform.TransformDirection(initialDirection);

		moveDirection.x = initialDirection.x * speed;
		moveDirection.z = initialDirection.z * speed;

		if (transform.position.y > ocean.transform.position.y)
		{
			if (characterController.isGrounded)
			{
				if (Input.GetButton("Jump"))
				{
					moveDirection.y = jumpSpeed;
				}
				else
				{
					moveDirection.y = -10f;
				}
			}
			else
			{
				moveDirection.y -= gravity * Time.deltaTime;
			}
		}
		else
		{
			if (Input.GetButton("Jump"))
			{
				moveDirection.y = jumpSpeed / 2;
			}
			else
			{
				moveDirection.y = -5f;
			}
		}

		characterController.Move(moveDirection * Time.deltaTime);
	}

    private void doAbility()
    {
        switch (currentAbility)
        {

            case "Trash Clearer":
                grenadeController.throwGrenade(characterCamera);
                currentAbility = "";
                break;
            case "Tide-turner":
                tideController.lowerTide();
                currentAbility = "";
                break;
            case "Auto-aim":
                // DO SOMETHING
                currentAbility = "";
                break;
            default:
                break;
        }
    }

}
