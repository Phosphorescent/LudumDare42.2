﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{

    public float tideSpeed = 1f;
    public float tideLowerY = -110f;
    public float tideUpperY = -45f;
    public float tideWeight = 0f;

    // Update is called once per frame
    void Update()
    {
        if (PointManager.preGame) return;
        float time = Time.time - PointManager.gameStartTime;

        float changeY = yChange(tideSpeed, time);
        changeY = (float)0.01 * changeY;

        transform.Translate(0, 0, changeY);
        // Calculate tideWeight
    }

    float yChange(float tideSpeed, float time)
    {
        return (tideSpeed + Mathf.Cos(time));
    }
}
