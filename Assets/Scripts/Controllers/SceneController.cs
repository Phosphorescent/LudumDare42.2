﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour {

    public Text quoteText;
    public string[] quotes;

    public GameObject howToScreen;
    public GameObject page1;
    public GameObject page2;

    public GameObject optionsMenu;

    public Slider sensitivitySlider;


    // Use this for initialization
    void Start () {

        int num = Random.Range(0, quotes.Length);
        Debug.Log(num);
        quoteText.text = quotes[num];
        if(PlayerPrefs.HasKey("customSens"))
        {
            sensitivitySlider.value = PlayerPrefs.GetFloat("customSens");
        } else
        {
            sensitivitySlider.value = 1f;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadMainScene()
    {
        SceneManager.LoadScene("Main");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void applyChanges()
    {
        PlayerPrefs.SetFloat("customSens", sensitivitySlider.value);
    }

    public void openOptions()
    {
        optionsMenu.SetActive(true);
    }

    public void returnFromOptions()
    {
        optionsMenu.SetActive(false);
    }

    public void openHelp()
    {
        howToScreen.SetActive(true);
        page1.SetActive(true);
        page2.SetActive(false);
    }

    public void toSecondPage()
    {
        page2.SetActive(true);
        page1.SetActive(false);
    }

    public void returnFromHelp()
    {
        howToScreen.SetActive(false);
    }

}
