﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour {

    public Transform rLeft;
    public Transform rRight;
    public Transform rTop;
    public Transform rBottom;

    public int trashAmount;
    public int environmentAmount;

    public GameObject[] trashPrefabs;
    public GameObject[] environmentPrefabs;

    private float minX;
    private float maxX;
    private float minZ;
    private float maxZ;


    // Use this for initialization
    void Start () {
        minX = rRight.position.x;
        maxX = rLeft.position.x;
        minZ = rTop.position.z;
        maxZ = rBottom.position.z;

		SpawnObjectSet();
	}
	
	void SpawnObjectSet()
	{
		SpawnObjects(trashPrefabs, trashAmount, 100f);
		SpawnObjects(environmentPrefabs, environmentAmount, 80f);
	}

    void SpawnObjects(GameObject[] prefArr, int amount, float height)
    {


        float xPos;
        float zPos;
        Vector3 startPos;

        for(int i = 0; i < amount-1; i++)
        {

            xPos = Random.Range(minX, maxX);
            zPos = Random.Range(minZ, maxZ);

            startPos = new Vector3(xPos, height, zPos);

            int num = Random.Range(0, prefArr.Length);

            GameObject trash = GameObject.Instantiate(prefArr[num], startPos, Random.rotation);

            trash.AddComponent(typeof(SpawnRigidbodyController));


        }

    }


}
