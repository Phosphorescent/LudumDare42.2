﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonWaterEffect : MonoBehaviour {
	public Transform oceanTransform;
	public GameObject waterEffect;
	
	// Update is called once per frame
	void Update () {
		if ( transform.position.y <= oceanTransform.position.y ) {
			waterEffect.SetActive(true);
		} else {
			waterEffect.SetActive(false);
		}
	}
}
