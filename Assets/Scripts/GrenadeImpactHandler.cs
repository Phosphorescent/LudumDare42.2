﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeImpactHandler : MonoBehaviour {

    public float pickupRadius = 5f;

    private Component pickupSphere;
    private bool isGrounded;

    public GameObject particles;
    public GameObject explosionSound;

	// Use this for initialization
	void Start () {
        // Create sphere collider	
        pickupSphere = this.gameObject.AddComponent(typeof(SphereCollider));
        SphereCollider newPickupSphere = (SphereCollider) pickupSphere;
        newPickupSphere.center = Vector3.zero;
        newPickupSphere.radius = pickupRadius;
        newPickupSphere.isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Beach")
        {
            isGrounded = true;
            GameObject explosion = GameObject.Instantiate(particles, this.transform.position, Quaternion.Euler(-90f, 0f, 0f));
            GameObject sound = GameObject.Instantiate(explosionSound, this.transform.position, Quaternion.identity);
            Destroy(this.transform.gameObject, 0.1f);
            Destroy(sound, 2f);
            Destroy(explosion, 4f);
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (isGrounded)
        {
            if (col.transform.tag == "Trash")
            {
                Destroy(col.transform.gameObject);
                PointManager.points += 1;
                PointManager.publicOpinion += 0.01f;
            }
        }
    }
}
