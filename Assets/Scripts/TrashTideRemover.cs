﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashTideRemover : MonoBehaviour {

    public GameObject splash;

	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Trash" && col as SphereCollider != null)
        {

            GameObject newSplash = GameObject.Instantiate(splash, col.transform.position, Quaternion.Euler(-90, 0, 0));
            Destroy(col.gameObject);
            Destroy(newSplash, 3);

			PointManager.publicOpinion -= 0.05f;

		}
    }
}
