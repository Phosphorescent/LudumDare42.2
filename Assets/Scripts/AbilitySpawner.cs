﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitySpawner : MonoBehaviour {

    public GameObject abilityPrefab;

    public Transform rLeft;
    public Transform rRight;
    public Transform rTop;
    public Transform rBottom;

    public GameObject notification;

    public int odds;

    private float startTime;

    private bool isSpawning = false;

    private float countdown;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(PointManager.preGame) { notification.SetActive(false);  return; }

        if (!isSpawning)
        {

            Random.InitState((int)Time.time);
            int num = Random.Range(0, odds);
            GameObject ability;

            if (num == 1)
            {
                float xPos = Random.Range(rRight.position.x, rLeft.position.x);
                float zPos = Random.Range(rTop.position.z, rBottom.position.z);

                Vector3 pos = new Vector3(xPos, 25f, zPos);

                ability = GameObject.Instantiate(abilityPrefab, pos, Quaternion.identity);
                isSpawning = true;
                startTime = Time.time;
                notification.SetActive(true);
            }

        } else
        {
            countdown = 3 - (Time.time - startTime);
            if (countdown <= 0)
            {
                notification.SetActive(false);
                isSpawning = false;
            }
        }
	}
}
